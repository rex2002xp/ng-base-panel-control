#### 0.0.1 (2019-04-27)

##### New Features

*  Tareas NPM para generacion del Archivo CHANGELOG.md ([465d7cbe](https://gitlab.com/RexSoftwareSolutions/solucion-de-aplicaciones-base/ng-base-panel-control/commit/465d7cbed686455008699f669c8f4d4027cd61c3))
*  Se agrego la libreria para la generacion del archivo CHANGELOG.md de forma automatica. ([bb676303](https://gitlab.com/RexSoftwareSolutions/solucion-de-aplicaciones-base/ng-base-panel-control/commit/bb67630387edf0d705ddced3e05547bf1f7bd1d9))

